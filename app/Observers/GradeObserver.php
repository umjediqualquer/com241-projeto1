<?php

namespace App\Observers;

use App\Models\Grade;

class GradeObserver
{
    /**
     * Handle the grade "created" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function created(Grade $grade)
    {
        //
    }

    /**
     * Handle the grade "updated" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function updated(Grade $grade)
    {
        //
    }

    /**
     * Handle the grade "saved" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function saved(Grade $grade)
    {
        $space = $grade->space;
        $space->wifi_speed = $space->grades->avg('wifi_speed');
        $space->save();
    }

    /**
     * Handle the grade "deleted" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function deleted(Grade $grade)
    {
        $space = $grade->space;

        if ($space->grades->count() != 0) {
            $space->wifi_speed = $space->grades->avg('wifi_speed');
        } else {
            $space->wifi_speed = $space->original_wifi_speed;
        }

        $space->save();
    }

    /**
     * Handle the grade "restored" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function restored(Grade $grade)
    {
        //
    }

    /**
     * Handle the grade "force deleted" event.
     *
     * @param  \App\Models\Grade  $grade
     * @return void
     */
    public function forceDeleted(Grade $grade)
    {
        //
    }
}
