<?php

namespace App\DataTables;

use DB;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Services\DataTable;

class SpaceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\DataTables\Datatables
     */
    public function dataTable()
    {
        $spaces = DB::table('spaces')
            ->select(
                "spaces.*",
                DB::raw("(
                            SELECT wifi_types.name
                            FROM wifi_types
                            WHERE wifi_types.id = spaces.wifi_type_id
                        ) as wifi_type_name"),
                DB::raw("(
                            SELECT users.name
                            FROM users
                            WHERE users.id = spaces.user_id
                        ) as user_name"),
                DB::raw("CONCAT(FORMAT(spaces.wifi_speed, 2, 'de_DE')) as readable_wifi_speed"),
                DB::raw("CONCAT(spaces.address, ', Nº',
                            CONCAT(spaces.number, ' - ',
                                CONCAT(spaces.neighborhood, ' - ',
                                    CONCAT(spaces.city, '/',
                                        CONCAT(spaces.state, ' (',
                                            CONCAT(spaces.zipcode, ')')
                                        )
                                    )
                                )
                            )
                        ) as full_address"),
            );

        return DataTables::of($spaces)
            ->filterColumn('wifi_type_name', function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT wifi_types.name
                                    FROM wifi_types
                                    WHERE wifi_types.id = spaces.wifi_type_id
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('user_name', function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT users.name
                                    FROM users
                                    WHERE users.id = spaces.user_id
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('wifi_speed', function($query, $keyword){
                $query->whereRaw("CONCAT(FORMAT(spaces.wifi_speed, 2, 'de_DE')) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('full_address', function ($query, $keyword) {
                $query->whereRaw("CONCAT(spaces.address, ', Nº',
                                    CONCAT(spaces.number, ' - ',
                                        CONCAT(spaces.neighborhood, ' - ',
                                            CONCAT(spaces.city, '/',
                                                CONCAT(spaces.state, ' (',
                                                    CONCAT(spaces.zipcode, ')')
                                                )
                                            )
                                        )
                                    )
                                ) like ?", ["%{$keyword}%"]);
            })
            ->addColumn("action", "spaces.datatables_actions")
            ->rawColumns(["action"]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->minifiedAjax()
            ->columns($this->getColumns())
            ->addAction(["width" => "75px", "printable" => false, "title" => \Lang::get("datatables.action")])
            ->parameters(DataTablesDefaults::getParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            "wifi_type_name"      => ["name" => "wifi_type_name", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.wifi_type_id")],
            "user_name"           => ["name" => "user_name",      "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.user_id")],
            "name"                => ["name" => "name",           "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.name")],
            "wifi_name"           => ["name" => "wifi_name",      "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.wifi_name")],
            "wifi_password"       => ["name" => "wifi_password",  "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.wifi_password")],
            "readable_wifi_speed" => ["name" => "wifi_speed",     "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.wifi_speed")],
            "sockets_number"      => ["name" => "sockets_number", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.sockets_number")],
            "full_address"        => ["name" => "full_address",   "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.full_address")],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'spaces_' . time();
    }
}