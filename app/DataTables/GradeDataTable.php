<?php

namespace App\DataTables;

use DB;
use App\Services\DataTablesDefaults;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Services\DataTable;
use Auth;

class GradeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\DataTables\Datatables
     */
    public function dataTable()
    {
        $grades = DB::table('grades')
            ->select(
                "grades.*",
                DB::raw("(
                            SELECT users.name
                            FROM users
                            WHERE users.id = grades.user_id
                        ) as user_name"),
                DB::raw("(
                            SELECT spaces.name
                            FROM spaces
                            WHERE spaces.id = grades.space_id
                        ) as space_name"),
                DB::raw("FORMAT(grades.wifi_speed, 2, 'de_DE') as readable_wifi_speed"),
            );

        if(Auth::user()->hasRole('user')) {
            $grades = $grades->where('user_id', Auth::user()->id);
        }

        return DataTables::of($grades)
            ->filterColumn('user_name', function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT users.name
                                    FROM users
                                    WHERE users.id = grades.user_id
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('space_name', function ($query, $keyword) {
                $query->whereRaw("(
                                    SELECT spaces.name
                                    FROM spaces
                                    WHERE spaces.id = grades.space_id
                                ) like ?", ["%{$keyword}%"]);
            })
            ->filterColumn('wifi_speed', function ($query, $keyword) {
                $query->whereRaw("FORMAT(grades.wifi_speed, 2, 'de_DE') like ?", ["%{$keyword}%"]);
            })
            ->addColumn("action", "grades.datatables_actions")
            ->rawColumns(["action"]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->minifiedAjax()
            ->columns($this->getColumns())
            ->addAction(["width" => "75px", "printable" => false, "title" => \Lang::get("datatables.action")])
            ->parameters(DataTablesDefaults::getParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            "user_name"           => ["name" => "user_name",  "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.user_id")],
            "space_name"          => ["name" => "space_name", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.space_id")],
            "rating"              => ["name" => "rating",     "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.rating")],
            "readable_wifi_speed" => ["name" => "wifi_speed", "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.wifi_speed")],
            "comments"            => ["name" => "comments",   "render" => "(data!=null)? ((data.length>180)? data.substr(0,180)+'...' : data) : '-'", "title" => \Lang::get("attributes.comments")],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'grades_' . time();
    }
}
