<?php

namespace App\Repositories;

use App\Models\WifiType;
use App\Repositories\BaseRepository;

/**
 * Class WifiTypeRepository
 * @package App\Repositories
 * @version October 18, 2020, 5:22 pm -03
*/

class WifiTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WifiType::class;
    }
}
