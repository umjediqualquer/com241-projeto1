<?php

namespace App\Repositories;

use App\Models\Space;
use App\Repositories\BaseRepository;

/**
 * Class SpaceRepository
 * @package App\Repositories
 * @version October 17, 2020, 9:53 pm -03
*/

class SpaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'wifi_type_id',
        'user_id',
        'name',
        'wifi_name',
        'wifi_password',
        'wifi_speed',
        'sockets_number',
        'zipcode',
        'address',
        'number',
        'neighborhood',
        'city',
        'state',
        'complement',
        'reference',
        'latitude',
        'longitude'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Space::class;
    }
}
