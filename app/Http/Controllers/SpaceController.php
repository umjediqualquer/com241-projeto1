<?php

namespace App\Http\Controllers;

use App\DataTables\SpaceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSpaceRequest;
use App\Http\Requests\UpdateSpaceRequest;
use App\Repositories\SpaceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\WifiType;
use App\Models\User;
use Auth;

class SpaceController extends AppBaseController
{
    /** @var  SpaceRepository */
    private $spaceRepository;

    public function __construct(SpaceRepository $spaceRepo)
    {
        $this->spaceRepository = $spaceRepo;
    }

    /**
     * Display a listing of the Space.
     *
     * @param SpaceDataTable $spaceDataTable
     * @return Response
     */
    public function index(SpaceDataTable $spaceDataTable)
    {
        return $spaceDataTable->render('spaces.index');
    }

    /**
     * Show the form for creating a new Space.
     *
     * @return Response
     */
    public function create()
    {
        $wifiTypesArray = WifiType::orderBy('id')->pluck('name', 'id')->toArray();
        $usersArray = User::role('user')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();

        return view('spaces.create', compact('wifiTypesArray', 'usersArray'));
    }

    /**
     * Store a newly created Space in storage.
     *
     * @param CreateSpaceRequest $request
     *
     * @return Response
     */
    public function store(CreateSpaceRequest $request)
    {
        $input = $request->all();

        if (in_array($input['wifi_type_id'], [1, 3])) {
            unset($input['wifi_password']);
        }

        $input['original_wifi_speed'] = $input['wifi_speed'];

        $space = $this->spaceRepository->create($input);

        Flash::success(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.saved", "m"));

        if (Auth::user()->hasRole('admin')) {
            $route = route('spaces.index');
        } else {
            $route = route('users.dashboard');
        }
        return redirect($route);
    }

    /**
     * Display the specified Space.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $space = $this->spaceRepository->find($id);

        if (empty($space)) {
            Flash::error(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('spaces.index'));
        }

        return view('spaces.show')->with('space', $space);
    }

    /**
     * Show the form for editing the specified Space.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $space = $this->spaceRepository->find($id);

        if (empty($space)) {
            Flash::error(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('spaces.index'));
        }

        $wifiTypesArray = WifiType::orderBy('id')->pluck('name', 'id')->toArray();
        $usersArray = User::role('user')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();

        return view('spaces.edit', compact('space', 'wifiTypesArray', 'usersArray'));
    }

    /**
     * Update the specified Space in storage.
     *
     * @param  int              $id
     * @param UpdateSpaceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpaceRequest $request)
    {
        $space = $this->spaceRepository->find($id);

        if (empty($space)) {
            Flash::error(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('spaces.index'));
        }

        $input = $request->all();

        if (in_array($input['wifi_type_id'], [1, 3])) {
            unset($input['wifi_password']);
        }

        $space = $this->spaceRepository->update($input, $id);

        Flash::success(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.updated", "m"));

        return redirect(route('spaces.index'));
    }

    /**
     * Remove the specified Space from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $space = $this->spaceRepository->find($id);

        if (empty($space)) {
            Flash::error(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('spaces.index'));
        }

        $this->spaceRepository->delete($id);

        Flash::success(\Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.deleted", "m"));

        return redirect(route('spaces.index'));
    }

    public function hasUserRated($id)
    {
        $space = $this->spaceRepository->find($id);

        if (empty($space)) {
            return response()->json([
                "message"  => \Lang::choice("tables.spaces", "s")." ".\Lang::choice("flash.not_found", "m")
            ], 422);
        }

        $data['hasUserRated'] = $space->grades->where('user_id', Auth::user()->id)->isNotEmpty();
        $data['gradeId'] = $space->grades->where('user_id', Auth::user()->id)->isNotEmpty()? $space->grades->where('user_id', Auth::user()->id)->first()->value('id') : null;

        return json_encode($data);
    }
}
