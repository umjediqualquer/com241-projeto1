<?php

namespace App\Http\Controllers;

use App\DataTables\WifiTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateWifiTypeRequest;
use App\Http\Requests\UpdateWifiTypeRequest;
use App\Repositories\WifiTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class WifiTypeController extends AppBaseController
{
    /** @var  WifiTypeRepository */
    private $wifiTypeRepository;

    public function __construct(WifiTypeRepository $wifiTypeRepo)
    {
        $this->wifiTypeRepository = $wifiTypeRepo;
    }

    /**
     * Display a listing of the WifiType.
     *
     * @param WifiTypeDataTable $wifiTypeDataTable
     * @return Response
     */
    public function index(WifiTypeDataTable $wifiTypeDataTable)
    {
        return $wifiTypeDataTable->render('wifi_types.index');
    }

    /**
     * Show the form for creating a new WifiType.
     *
     * @return Response
     */
    public function create()
    {
        return view('wifi_types.create');
    }

    /**
     * Store a newly created WifiType in storage.
     *
     * @param CreateWifiTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateWifiTypeRequest $request)
    {
        $input = $request->all();

        $wifiType = $this->wifiTypeRepository->create($input);

        Flash::success(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.saved", "m"));

        return redirect(route('wifiTypes.index'));
    }

    /**
     * Display the specified WifiType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wifiType = $this->wifiTypeRepository->find($id);

        if (empty($wifiType)) {
            Flash::error(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('wifiTypes.index'));
        }

        return view('wifi_types.show')->with('wifiType', $wifiType);
    }

    /**
     * Show the form for editing the specified WifiType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wifiType = $this->wifiTypeRepository->find($id);

        if (empty($wifiType)) {
            Flash::error(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('wifiTypes.index'));
        }

        return view('wifi_types.edit')->with('wifiType', $wifiType);
    }

    /**
     * Update the specified WifiType in storage.
     *
     * @param  int              $id
     * @param UpdateWifiTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWifiTypeRequest $request)
    {
        $wifiType = $this->wifiTypeRepository->find($id);

        if (empty($wifiType)) {
            Flash::error(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('wifiTypes.index'));
        }

        $wifiType = $this->wifiTypeRepository->update($request->all(), $id);

        Flash::success(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.updated", "m"));

        return redirect(route('wifiTypes.index'));
    }

    /**
     * Remove the specified WifiType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wifiType = $this->wifiTypeRepository->find($id);

        if (empty($wifiType)) {
            Flash::error(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('wifiTypes.index'));
        }

        $this->wifiTypeRepository->delete($id);

        Flash::success(\Lang::choice("tables.wifi_types", "s")." ".\Lang::choice("flash.deleted", "m"));

        return redirect(route('wifiTypes.index'));
    }
}
