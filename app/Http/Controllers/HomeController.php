<?php

namespace App\Http\Controllers;

use Auth;
use Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch (Auth::user()->roles->first()->id) {
            case config("enums.roles.ADMIN.id"):
                if (Request::routeIs(["home"])) {
                    return response(view("home"));
                } else {
                    return redirect(route("home"));
                }
            break;

            case config("enums.roles.USER.id"):
                return redirect(route("users.dashboard"));
            break;
        }
    }
}
