<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Spatie\Permission\Models\Role;
use App\Models\Space;
use App\Models\WifiType;
use Auth;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function dashboard()
    {
        $latitude = -22.412438;
        $longitude = -45.449813;
        $spaces = Space::nearBy($latitude, $longitude)->get();

        foreach ($spaces as $space) {
            $space->maps_url = "http://maps.google.com/maps?q={$space->latitude},{$space->longitude}";
            $space->fullStars = round($space->grades->avg('rating'));
            $space->emptyStars = 5 - $space->fullStars;
        }

        return view('users.dashboard', compact('spaces'));
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $rolesArray = Role::orderBy('id')->pluck('display_name', 'name')->toArray();

        return view('users.create', compact('rolesArray'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $user = $this->userRepository->create($input);
        $user->assignRole($input["role_name"]);

        Flash::success(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.saved", "m"));

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);

        if (Auth::user()->hasRole('user') && Auth::user()->id != $id) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.dashboard'));
        }

        if (empty($user)) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.index'));
        }

        $rolesArray = Role::orderBy('id')->pluck('display_name', 'name')->toArray();

        return view('users.edit', compact('user', 'rolesArray'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);

        if (Auth::user()->hasRole('user') && Auth::user()->id != $id) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.dashboard'));
        }

        if (empty($user)) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.index'));
        }

        $input = $request->all();

        $user = $this->userRepository->update($input, $id);
        if (Auth::user()->hasRole('admin')) {
            $user->syncRoles($input['role_name']);
        }

        Flash::success(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.updated", "m"));

        if (Auth::user()->hasRole('admin')) {
            return redirect(route('users.index'));
        } else {
            return redirect(route('users.dashboard'));
        }
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success(\Lang::choice("tables.users", "s")." ".\Lang::choice("flash.deleted", "m"));

        return redirect(route('users.index'));
    }
}
