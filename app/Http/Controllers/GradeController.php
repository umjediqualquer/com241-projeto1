<?php

namespace App\Http\Controllers;

use App\DataTables\GradeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGradeRequest;
use App\Http\Requests\UpdateGradeRequest;
use App\Repositories\GradeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\User;
use App\Models\Space;
use Auth;
use App\Models\Grade;

class GradeController extends AppBaseController
{
    /** @var  GradeRepository */
    private $gradeRepository;

    public function __construct(GradeRepository $gradeRepo)
    {
        $this->gradeRepository = $gradeRepo;
    }

    /**
     * Display a listing of the Grade.
     *
     * @param GradeDataTable $gradeDataTable
     * @return Response
     */
    public function index(GradeDataTable $gradeDataTable)
    {
        return $gradeDataTable->render('grades.index');
    }

    /**
     * Show the form for creating a new Grade.
     *
     * @return Response
     */
    public function create()
    {
        $usersArray = User::role('user')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        if (Auth::user()->hasRole('admin')) {
            $spacesArray = Space::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        } else {
            $spacesArray = Space::whereNotIn('id', Auth::user()->grades->pluck('space_id'))->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        }

        return view('grades.create', compact('usersArray', 'spacesArray'));
    }

    /**
     * Store a newly created Grade in storage.
     *
     * @param CreateGradeRequest $request
     *
     * @return Response
     */
    public function store(CreateGradeRequest $request)
    {
        if (Auth::user()->hasRole('admin')) {
            $route = route('grades.index');
        } else {
            $route = route('user.grades.index');
        }

        $input = $request->all();

        $grade = $this->gradeRepository->create($input);

        Flash::success(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.saved", "m"));

        return redirect($route);
    }

    /**
     * Display the specified Grade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (Auth::user()->hasRole('admin')) {
            $grade = $this->gradeRepository->find($id);
            $route = route('grades.index');
        } else {
            $route = route('user.grades.index');
            $grade = Grade::where('user_id', Auth::user()->id)->find($id);
        }

        if (empty($grade)) {
            Flash::error(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect($route);
        }

        return view('grades.show')->with('grade', $grade);
    }

    /**
     * Show the form for editing the specified Grade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->hasRole('admin')) {
            $grade = $this->gradeRepository->find($id);
            $route = route('grades.index');
        } else {
            $route = route('user.grades.index');
            $grade = Grade::where('user_id', Auth::user()->id)->find($id);
        }

        if (empty($grade)) {
            Flash::error(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect($route);
        }

        $usersArray = User::role('user')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        if (Auth::user()->hasRole('admin')) {
            $spacesArray = Space::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        } else {
            $spacesArray = Space::where('id', $grade->space_id)->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        }

        return view('grades.edit', compact('grade', 'usersArray', 'spacesArray'));
    }

    /**
     * Update the specified Grade in storage.
     *
     * @param  int              $id
     * @param UpdateGradeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGradeRequest $request)
    {
        if (Auth::user()->hasRole('admin')) {
            $grade = $this->gradeRepository->find($id);
            $route = route('grades.index');
        } else {
            $route = route('user.grades.index');
            $grade = Grade::where('user_id', Auth::user()->id)->find($id);
        }

        if (empty($grade)) {
            Flash::error(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect($route);
        }

        $grade = $this->gradeRepository->update($request->all(), $id);

        Flash::success(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.updated", "m"));

        return redirect($route);
    }

    /**
     * Remove the specified Grade from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->hasRole('admin')) {
            $grade = $this->gradeRepository->find($id);
            $route = route('grades.index');
        } else {
            $route = route('user.grades.index');
            $grade = Grade::where('user_id', Auth::user()->id)->find($id);
        }

        if (empty($grade)) {
            Flash::error(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.not_found", "m"));

            return redirect($route);
        }

        $this->gradeRepository->delete($id);

        Flash::success(\Lang::choice("tables.grades", "s")." ".\Lang::choice("flash.deleted", "m"));

        return redirect($route);
    }
}
