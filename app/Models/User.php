<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Helpers\Functions;

/**
 * Class User
 * @package App\Models
 * @version October 17, 2020, 9:12 pm -03
 *
 * @property \Illuminate\Database\Eloquent\Collection $grades
 * @property \Illuminate\Database\Eloquent\Collection $spaces
 * @property string $name
 * @property string $email
 * @property string $password
 * @property boolean $is_active
 */
class User extends Authenticatable
{
    use HasRoles;

    public $table = 'users';




    public $fillable = [
        'name',
        'email',
        'password',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'name'              => 'string',
        'email'             => 'string',
        'password'          => 'string',
        'is_active'         => 'boolean',
        'email_verified_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'      => 'required|string|max:255',
        'email'     => 'required|string|email|max:255|unique:users,email,id_to_ignore,id',
        'password'  => 'required|string|min:6|confirmed',
        'is_active' => 'required',
    ];

    /**
     * Registration rules
     *
     * @var array
     */
    public static $registrationRules = [
        'name'     => 'required|string|max:255',
        'email'    => 'required|string|email|max:255|unique:users,email,id_to_ignore,id',
        'password' => 'required|string|min:6|confirmed',
    ];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        'readable_created_at',
        'readable_updated_at',
        'readable_email_verified_at',
        'readable_is_active',
        'readable_role_name',
        'role_name',
    ];

    // =========================================================================
    // Relationships
    // =========================================================================

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function grades()
    {
        return $this->hasMany(\App\Models\Grade::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function spaces()
    {
        return $this->hasMany(\App\Models\Space::class, 'user_id');
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get readable_created_at
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }

    /**
     * Get readable_email_verified_at
     *
     * @return string
     */
    public function getReadableEmailVerifiedAtAttribute()
    {
        return Functions::formatDatetime($this->email_verified_at);
    }

    /**
     * Get readable_is_active
     *
     * @return string
     */
    public function getReadableIsActiveAttribute()
    {
        return Functions::formatBoolean($this->is_active);
    }

    /**
     * Get role_name
     *
     * @return string
     */
    public function getRoleNameAttribute()
    {
        return $this->roles->first()->name;
    }

    /**
     * Get readable_role_name
     *
     * @return string
     */
    public function getReadableRoleNameAttribute()
    {
        return $this->roles->first()->display_name;
    }

    // =========================================================================
    // Setters
    // =========================================================================

    /**
     * Set password
     *
     * @param string $value
     *
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $value? $this->attributes['password'] = bcrypt($value) : null;
    }
}
