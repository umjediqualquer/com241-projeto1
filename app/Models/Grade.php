<?php

namespace App\Models;

use Eloquent as Model;
use App\Helpers\Functions;

/**
 * Class Grade
 * @package App\Models
 * @version October 17, 2020, 9:58 pm -03
 *
 * @property \App\Models\User $user
 * @property \App\Models\Space $space
 * @property integer $user_id
 * @property integer $space_id
 * @property integer $rating
 * @property number $wifi_speed
 * @property string $comments
 */
class Grade extends Model
{

    public $table = 'grades';




    public $fillable = [
        'user_id',
        'space_id',
        'rating',
        'wifi_speed',
        'comments'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'space_id' => 'integer',
        'rating' => 'integer',
        'wifi_speed' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'space_id' => 'required',
        'rating' => 'required',
        'wifi_speed' => 'required'
    ];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        'readable_created_at',
        'readable_updated_at',
        'readable_wifi_speed',
    ];

    // =========================================================================
    // Relationships
    // =========================================================================

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function space()
    {
        return $this->belongsTo(\App\Models\Space::class, 'space_id', 'id');
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get readable_created_at
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }

    /**
     * Get readable_wifi_speed
     *
     * @return string
     */
    public function getReadableWifiSpeedAttribute()
    {
        return Functions::formatDecimal($this->wifi_speed);
    }
}
