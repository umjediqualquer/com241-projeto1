<?php

namespace App\Models;

use Eloquent as Model;
use App\Helpers\Functions;

/**
 * Class WifiType
 * @package App\Models
 * @version October 18, 2020, 5:22 pm -03
 *
 * @property \Illuminate\Database\Eloquent\Collection $spaces
 * @property string $name
 * @property string $description
 */
class WifiType extends Model
{

    public $table = 'wifi_types';




    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        'readable_created_at',
        'readable_updated_at',
    ];

    // =========================================================================
    // Relationships
    // =========================================================================

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function spaces()
    {
        return $this->hasMany(\App\Models\Space::class, 'wifi_type_id');
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get readable_created_at
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }
}
