<?php

namespace App\Models;

use Eloquent as Model;
use App\Helpers\Functions;
use DB;

/**
 * Class Space
 * @package App\Models
 * @version October 17, 2020, 9:53 pm -03
 *
 * @property \App\Models\WifiType $wifi_type
 * @property \App\Models\User $user
 * @property integer $user_id
 * @property string $name
 * @property string $wifi_password
 * @property number $wifi_speed
 * @property integer $sockets_number
 * @property string $zipcode
 * @property string $address
 * @property string $number
 * @property string $neighborhood
 * @property string $city
 * @property string $state
 * @property string $complement
 * @property string $reference
 * @property decimal(10 $latitude
 * @property decimal(11 $longitude
 */
class Space extends Model
{

    public $table = 'spaces';




    public $fillable = [
        'wifi_type_id',
        'user_id',
        'name',
        'wifi_name',
        'wifi_password',
        'original_wifi_speed',
        'wifi_speed',
        'sockets_number',
        'zipcode',
        'address',
        'number',
        'neighborhood',
        'city',
        'state',
        'complement',
        'reference',
        'latitude',
        'longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'wifi_type_id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'wifi_name' => 'string',
        'wifi_password' => 'string',
        'original_wifi_speed' => 'float',
        'wifi_speed' => 'float',
        'sockets_number' => 'integer',
        'zipcode' => 'string',
        'address' => 'string',
        'number' => 'string',
        'neighborhood' => 'string',
        'city' => 'string',
        'state' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'wifi_type_id' => 'required',
        'user_id' => 'required',
        'name' => 'required',
        'wifi_name' => 'required',
        'wifi_speed' => 'required',
        'sockets_number' => 'required',
        'zipcode' => 'required',
        'address' => 'required',
        'number' => 'required',
        'neighborhood' => 'required',
        'city' => 'required',
        'state' => 'required'
    ];

    /**
     * The accessors to append to the model's array form
     *
     * @var array
     */
    protected $appends = [
        'readable_created_at',
        'readable_updated_at',
        'readable_latitude',
        'readable_longitude',
        'full_address',
        'readable_wifi_speed',
    ];

    // =========================================================================
    // Relationships
    // =========================================================================

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wifiType()
    {
        return $this->belongsTo(\App\Models\WifiType::class, 'wifi_type_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function grades()
    {
        return $this->hasMany(\App\Models\Grade::class, 'space_id');
    }

    // =========================================================================
    // Getters
    // =========================================================================

    /**
     * Get readable_created_at
     *
     * @return string
     */
    public function getReadableCreatedAtAttribute()
    {
        return Functions::formatDatetime($this->created_at);
    }

    /**
     * Get readable_updated_at
     *
     * @return string
     */
    public function getReadableUpdatedAtAttribute()
    {
        return Functions::formatDatetime($this->updated_at);
    }

    /**
     * Get readable_latitude
     *
     * @return string
     */
    public function getReadableLatitudeAttribute()
    {
        return Functions::formatCoordinate($this->latitude);
    }

    /**
     * Get readable_longitude
     *
     * @return string
     */
    public function getReadableLongitudeAttribute()
    {
        return Functions::formatCoordinate($this->longitude);
    }

    /**
     * Get full_address
     *
     * @return string
     */
    public function getFullAddressAttribute()
    {
        $fullAddress = "";
        $fullAddress .= empty($this->address)? "" : $this->address;
        $fullAddress .= empty($this->number)? "" : (empty($fullAddress)? $this->number : ", Nº $this->number");
        $fullAddress .= empty($this->neighborhood)? "" : (empty($fullAddress)? $this->neighborhood : " - $this->neighborhood");
        $fullAddress .= empty($this->city)? "" : (empty($fullAddress)? $this->city : " - $this->city");
        $fullAddress .= empty($this->state)? "" : (empty($fullAddress)? $this->state : "/$this->state");
        $fullAddress .= empty($this->zipcode)? "" : (empty($fullAddress)? $this->zipcode : " ($this->zipcode)");

        return empty($fullAddress)? null : $fullAddress;
    }

    public function scopeNearBy($query, float $latitude, float $longitude)
    {
        return $query->select(
            DB::raw("*, ROUND((
                6371 * acos(
                    cos( radians(  ?  ) ) *
                    cos( radians( latitude ) ) *
                    cos( radians( longitude ) - radians(?) ) +
                    sin( radians(  ?  ) ) *
                    sin( radians( latitude ) )
                )
                ), 3) AS distance")
        )
        ->orderBy("distance")
        ->setBindings([$latitude, $longitude, $latitude]);
    }

    /**
     * Get readable_wifi_speed
     *
     * @return string
     */
    public function getReadableWifiSpeedAttribute()
    {
        return Functions::formatDecimal($this->wifi_speed);
    }

    // =========================================================================
    // Setters
    // =========================================================================

    /**
     * Set wifi_password
     *
     * @param string $value
     *
     * @return void
     */
    public function setWifiPasswordAttribute($value)
    {
        $this->attributes['wifi_password'] = (in_array(request()->wifi_type_id, [1, 3]))? null : $value;
    }
}
