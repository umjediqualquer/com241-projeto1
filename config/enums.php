<?php

return [
    'roles' =>
    [
        'ADMIN' => ['id' => 1, 'name' => 'admin', 'display_name' => 'Administrador', 'guard_name' => 'web'],
        'USER'  => ['id' => 2, 'name' => 'user',  'display_name' => 'Usuário',       'guard_name' => 'web'],
    ],

    'wifi_types' =>
    [
        'OPEN'     => ['id' => 1, 'name' => 'Rede aberta',             'description' => 'Rede aberta, sem senha ou registro/login.'],
        'PASSWORD' => ['id' => 2, 'name' => 'Rede com senha',          'description' => 'Rede protegida com senha.'],
        'LOGIN'    => ['id' => 3, 'name' => 'Rede com registro/login', 'description' => 'Rede sem senha, mas que exige um registro ou login.'],
    ],
];
