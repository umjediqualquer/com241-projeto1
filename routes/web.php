<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', function () {
    return redirect(route("home"));
});

Route::group(["middleware" => ["auth", "active"]], function () {
    // Home
    Route:: get("/home",                                ["as" => "home", "uses" => "HomeController@index"]);

    // Address
    Route::group(['prefix' => 'address'], function () {
        Route:: get('/find-by-zipcode/{zipcode}', ['as'=>'address.findByZipcode', 'uses'=>'AddressController@findByZipcode']);
    });

    Route::group(['middleware' => ['role:admin']], function () {
        // User
        Route::group(["prefix" => "users"], function () {
            Route::get("/",                              ["as"=>"users.index",   "uses"=>"UserController@index"]);
            Route::get("/create",                        ["as"=>"users.create",  "uses"=>"UserController@create"]);
            Route::post("/",                             ["as"=>"users.store",   "uses"=>"UserController@store"]);
            Route::get("/{user_id}",                     ["as"=>"users.show",    "uses"=>"UserController@show"]);
            Route::match(["put", "patch"], "/{user_id}", ["as"=>"users.update",  "uses"=>"UserController@update"]);
            Route::delete("/{user_id}",                  ["as"=>"users.destroy", "uses"=>"UserController@destroy"]);
            Route::get("/{user_id}/edit",                ["as"=>"users.edit",    "uses"=>"UserController@edit"]);
        });

        // WifiType
        Route::group(["prefix" => "wifi-types"], function () {
            Route::get("/",                                   ["as"=>"wifiTypes.index",   "uses"=>"WifiTypeController@index"]);
            Route::get("/create",                             ["as"=>"wifiTypes.create",  "uses"=>"WifiTypeController@create"]);
            Route::post("/",                                  ["as"=>"wifiTypes.store",   "uses"=>"WifiTypeController@store"]);
            Route::get("/{wifi_type_id}",                     ["as"=>"wifiTypes.show",    "uses"=>"WifiTypeController@show"]);
            Route::match(["put", "patch"], "/{wifi_type_id}", ["as"=>"wifiTypes.update",  "uses"=>"WifiTypeController@update"]);
            Route::delete("/{wifi_type_id}",                  ["as"=>"wifiTypes.destroy", "uses"=>"WifiTypeController@destroy"]);
            Route::get("/{wifi_type_id}/edit",                ["as"=>"wifiTypes.edit",    "uses"=>"WifiTypeController@edit"]);
        });

        // Space
        Route::group(["prefix" => "spaces"], function () {
            Route::get("/",                               ["as"=>"spaces.index",   "uses"=>"SpaceController@index"]);
            Route::get("/create",                         ["as"=>"spaces.create",  "uses"=>"SpaceController@create"]);
            Route::post("/",                              ["as"=>"spaces.store",   "uses"=>"SpaceController@store"]);
            Route::get("/{space_id}",                     ["as"=>"spaces.show",    "uses"=>"SpaceController@show"]);
            Route::match(["put", "patch"], "/{space_id}", ["as"=>"spaces.update",  "uses"=>"SpaceController@update"]);
            Route::delete("/{space_id}",                  ["as"=>"spaces.destroy", "uses"=>"SpaceController@destroy"]);
            Route::get("/{space_id}/edit",                ["as"=>"spaces.edit",    "uses"=>"SpaceController@edit"]);
        });

        // Grade
        Route::group(["prefix" => "grades"], function () {
            Route::get("/",                               ["as"=>"grades.index",   "uses"=>"GradeController@index"]);
            Route::get("/create",                         ["as"=>"grades.create",  "uses"=>"GradeController@create"]);
            Route::post("/",                              ["as"=>"grades.store",   "uses"=>"GradeController@store"]);
            Route::get("/{grade_id}",                     ["as"=>"grades.show",    "uses"=>"GradeController@show"]);
            Route::match(["put", "patch"], "/{grade_id}", ["as"=>"grades.update",  "uses"=>"GradeController@update"]);
            Route::delete("/{grade_id}",                  ["as"=>"grades.destroy", "uses"=>"GradeController@destroy"]);
            Route::get("/{grade_id}/edit",                ["as"=>"grades.edit",    "uses"=>"GradeController@edit"]);
        });
    });

    Route::group(['middleware' => ['role:user']], function () {
        Route:: get("/dashboard",                                ["as" => "users.dashboard", "uses" => "UserController@dashboard"]);

        // User
        Route::group(["prefix" => "user-profile"], function () {
            Route::match(["put", "patch"], "/{user_id}", ["as"=>"user.users.update",  "uses"=>"UserController@update"]);
            Route::get("/{user_id}/edit",                ["as"=>"user.users.edit",    "uses"=>"UserController@edit"]);
        });

        // Space
        Route::group(["prefix" => "user-spaces"], function () {
            Route::get("/create",                         ["as"=>"user.spaces.create",       "uses"=>"SpaceController@create"]);
            Route::post("/",                              ["as"=>"user.spaces.store",        "uses"=>"SpaceController@store"]);
            Route::get("/{space_id}",                     ["as"=>"user.spaces.show",         "uses"=>"SpaceController@show"]);
            Route::get("/{space_id}/has-user-rated",      ["as"=>"user.spaces.hasUserRated", "uses"=>"SpaceController@hasUserRated"]);
        });

        // Grade
        Route::group(["prefix" => "user-grades"], function () {
            Route::get("/",                               ["as"=>"user.grades.index",   "uses"=>"GradeController@index"]);
            Route::get("/create",                         ["as"=>"user.grades.create",  "uses"=>"GradeController@create"]);
            Route::post("/",                              ["as"=>"user.grades.store",   "uses"=>"GradeController@store"]);
            Route::get("/{grade_id}",                     ["as"=>"user.grades.show",    "uses"=>"GradeController@show"]);
            Route::match(["put", "patch"], "/{grade_id}", ["as"=>"user.grades.update",  "uses"=>"GradeController@update"]);
            Route::delete("/{grade_id}",                  ["as"=>"user.grades.destroy", "uses"=>"GradeController@destroy"]);
            Route::get("/{grade_id}/edit",                ["as"=>"user.grades.edit",    "uses"=>"GradeController@edit"]);
        });
    });
});