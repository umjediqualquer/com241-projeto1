<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Space;
use Faker\Generator as Faker;

$factory->define(Space::class, function (Faker $faker) {

    return [
        'wifi_type_id' => $faker->randomDigitNotNull,
        'user_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'wifi_name' => $faker->word,
        'wifi_password' => $faker->word,
        'wifi_speed' => $faker->randomDigitNotNull,
        'sockets_number' => $faker->randomDigitNotNull,
        'zipcode' => $faker->word,
        'address' => $faker->word,
        'number' => $faker->word,
        'neighborhood' => $faker->word,
        'city' => $faker->word,
        'state' => $faker->word,
        'complement' => $faker->word,
        'reference' => $faker->word,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
