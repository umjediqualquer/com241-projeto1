<?php

use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentTimestamp = DB::raw('CURRENT_TIMESTAMP');

        // Users
        $users = [
            [
                'name'              => 'Nathan Otávio Ribeiro',
                'email'             => 'nathan@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Natália Maya Emanuelly dos Santos',
                'email'             => 'natalia@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Vicente Kevin Márcio da Luz',
                'email'             => 'vicente@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Marcela Clara Baptista',
                'email'             => 'marcela@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Isabelly Aparecida Isabel Almeida',
                'email'             => 'isabelly@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Fátima Francisca Ribeiro',
                'email'             => 'fatima@user.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
        ];
        DB::table('users')->insert($users);

        // Users Roles
        $usersRoles = [
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 4
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 5
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 6
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 7
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 8
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.USER.id'),
                'model_id'   => 9
            ],
        ];
        DB::table('model_has_roles')->insert($usersRoles);

        // Spaces
        $spaces = [
            [
                'wifi_type_id'        => 1,
                'user_id'             => 5,
                'name'                => 'Local 1',
                'wifi_name'           => 'local_1',
                'wifi_password'       => NULL,
                'original_wifi_speed' => 100,
                'wifi_speed'          => 100,
                'sockets_number'      => 5,
                'zipcode'             => '69401-089',
                'address'             => 'Rua Caapiranga',
                'number'              => '1',
                'neighborhood'        => 'União',
                'city'                => 'Manacapuru',
                'state'               => 'AM',
                'latitude'            => -3.27694997,
                'longitude'           => -60.62595991,
                'created_at'          => $currentTimestamp,
                'updated_at'          => $currentTimestamp,
            ],
            [
                'wifi_type_id'        => 2,
                'user_id'             => 7,
                'name'                => 'Local 2',
                'wifi_name'           => 'local_2',
                'wifi_password'       => 'bemvindoaolocal',
                'original_wifi_speed' => 200,
                'wifi_speed'          => 200,
                'sockets_number'      => 10,
                'zipcode'             => '68908-127',
                'address'             => 'Avenida General Osório',
                'number'              => '2',
                'neighborhood'        => 'Jesus de Nazaré',
                'city'                => 'Macapá',
                'state'               => 'AP',
                'latitude'            => 0.04845587,
                'longitude'           => -51.06337236,
                'created_at'          => $currentTimestamp,
                'updated_at'          => $currentTimestamp,
            ],
            [
                'wifi_type_id'        => 3,
                'user_id'             => 9,
                'wifi_name'           => 'local_3',
                'name'                => 'Local 3',
                'wifi_password'       => NULL,
                'original_wifi_speed' => 300,
                'wifi_speed'          => 300,
                'sockets_number'      => 15,
                'zipcode'             => '54230-630',
                'address'             => 'Rua Tartaruga',
                'number'              => '3',
                'neighborhood'        => 'Zumbi do Pacheco',
                'city'                => 'Jaboatão dos Guararapes',
                'state'               => 'PE',
                'latitude'            => -8.13362291,
                'longitude'           => -34.96344503,
                'created_at'          => $currentTimestamp,
                'updated_at'          => $currentTimestamp,
            ],
        ];
        DB::table('spaces')->insert($spaces);

        // Grades
        $grades = [
            [
                'user_id'    => 5,
                'space_id'   => 1,
                'rating'     => 2,
                'wifi_speed' => 75,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper consectetur libero id ullamcorper. Donec tempus dui eget erat accumsan vehicula. Donec vel nisl urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam rhoncus eros tortor, egestas pretium turpis venenatis et. Nullam molestie est ut odio fringilla, ac volutpat risus auctor. Praesent condimentum feugiat purus, at gravida urna tempor vel. In at neque pulvinar enim accumsan consectetur quis et quam. Suspendisse finibus ante orci, sollicitudin convallis urna aliquam et. Etiam sagittis, tellus quis convallis porttitor, sapien orci feugiat velit, ut semper eros magna vitae lacus. Mauris ex massa, hendrerit at sem ut, consequat efficitur magna. Proin elementum mi vitae nibh varius, quis finibus nunc tincidunt.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
            [
                'user_id'    => 6,
                'space_id'   => 1,
                'rating'     => 3,
                'wifi_speed' => 125,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at tempor ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam accumsan semper lectus, sed pulvinar quam pulvinar non. Aliquam et sollicitudin ex. Vestibulum lacinia metus eu rhoncus pretium. Nulla aliquam dui non faucibus feugiat. Curabitur tincidunt tristique diam, sit amet scelerisque ex auctor sit amet. Praesent placerat ultrices tellus ut volutpat. In non auctor dui. Quisque maximus pretium lectus, vitae sodales nibh lacinia non. Donec ipsum libero, lobortis ut facilisis vel, condimentum vitae tortor.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
            [
                'user_id'    => 7,
                'space_id'   => 2,
                'rating'     => 3,
                'wifi_speed' => 175,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae ornare lorem, eu pretium sem. Integer interdum, orci quis interdum pellentesque, orci diam fringilla odio, non convallis massa eros id ipsum. Curabitur nulla libero, dignissim at tristique eu, hendrerit id ligula. Ut gravida tristique nunc, et ultrices dolor dictum quis. Quisque sagittis ex efficitur augue euismod, quis congue ante pellentesque. Aenean rutrum urna ac dui elementum finibus eget ut mi. Phasellus massa ante, faucibus sed lacus sed, commodo vehicula erat. Maecenas vitae dictum augue, ac ultricies turpis. Vivamus ultrices tempus ante sit amet sollicitudin.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
            [
                'user_id'    => 8,
                'space_id'   => 2,
                'rating'     => 4,
                'wifi_speed' => 225,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pulvinar nec leo a rutrum. Suspendisse blandit enim augue, eget tincidunt ligula lacinia non. Phasellus ipsum mauris, consequat in mollis vel, semper ac leo. Pellentesque sagittis dolor non purus ultrices placerat. Mauris nisl est, condimentum vel justo eget, congue accumsan massa. Vestibulum eleifend elit sit amet diam fringilla hendrerit. Cras facilisis mollis convallis. In egestas quam lectus, ut tristique magna sagittis ullamcorper. Integer vehicula erat convallis, lobortis est vitae, gravida orci. Praesent tincidunt sapien ac ultrices imperdiet. Morbi semper ex ac tempus vulputate. Suspendisse malesuada nunc sit amet justo tempus, ut molestie magna ultricies. Vestibulum at odio lacinia, bibendum ligula non, scelerisque enim. Donec venenatis fringilla vulputate. Etiam massa diam, ultrices ac fringilla vel, laoreet quis arcu. Maecenas efficitur mauris id elit faucibus, vel auctor tortor faucibus.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
            [
                'user_id'    => 9,
                'space_id'   => 3,
                'rating'     => 4,
                'wifi_speed' => 275,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vulputate augue nec purus pretium pellentesque eget id arcu. Aliquam rhoncus nisi sit amet enim elementum tincidunt et eget leo. Ut non convallis arcu. Donec lobortis risus at nunc aliquam varius. Quisque lacinia neque sit amet tincidunt imperdiet. Sed efficitur et nisi vitae hendrerit. Mauris tincidunt lectus quis ante rutrum mattis. Maecenas id urna non mi molestie placerat in a odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean consectetur ex id libero varius aliquam.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
            [
                'user_id'    => 4,
                'space_id'   => 3,
                'rating'     => 5,
                'wifi_speed' => 325,
                'comments'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius sed magna non mattis. Suspendisse et placerat ipsum. Integer porta dapibus laoreet. Fusce ac sapien et leo finibus consequat. Nam vel dui neque. Nullam rutrum nibh tortor, a laoreet magna mattis eu. Morbi quis ex enim. Nulla vel cursus elit. Nunc lacinia interdum ipsum, eget mollis libero faucibus et. Praesent malesuada id quam sit amet iaculis. Fusce imperdiet nisi sed pretium dignissim. Praesent ornare placerat dui, eget interdum metus. Phasellus viverra velit metus, id sagittis felis auctor quis.',
                'created_at' => $currentTimestamp,
                'updated_at' => $currentTimestamp,
            ],
        ];
        DB::table('grades')->insert($grades);
    }
}
