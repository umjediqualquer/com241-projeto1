<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentTimestamp = DB::raw('CURRENT_TIMESTAMP');

        // Roles
        $roles = array_map(['App\Helpers\Functions', 'addTimestamp'], array_values(config('enums.roles')));
        DB::table('roles')->insert($roles);

        // Admins
        $admins = [
            [
                'name'              => 'Ádrian Castello',
                'email'             => 'adrian@admin.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'Giulia Rodrigues',
                'email'             => 'giulia@admin.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
            [
                'name'              => 'José Francisco',
                'email'             => 'jose@admin.com',
                'password'          => bcrypt('123456'),
                'is_active'         => true,
                'email_verified_at' => $currentTimestamp,
                'created_at'        => $currentTimestamp,
                'updated_at'        => $currentTimestamp,
            ],
        ];
        DB::table('users')->insert($admins);

        // Admins Roles
        $adminsRoles = [
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.ADMIN.id'),
                'model_id'   => 1
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.ADMIN.id'),
                'model_id'   => 2
            ],
            [
                'model_type' => 'App\Models\User',
                'role_id'    => config('enums.roles.ADMIN.id'),
                'model_id'   => 3
            ],
        ];
        DB::table('model_has_roles')->insert($adminsRoles);

        // Wifi Types
        $wifiTypes = array_map(['App\Helpers\Functions', 'addTimestamp'], array_values(config('enums.wifi_types')));
        DB::table('wifi_types')->insert($wifiTypes);
    }
}
