@push('css')
    <!-- Leaflet v1.6.0 -->
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/leaflet.css') }}">

    <!-- Esri Leaflet Geocoder v2.3.2 -->
    <link rel="stylesheet" href="{{ asset('vendor/leaflet/plugins/esri-leaflet-geocoder.css') }}">

    <!-- Map CSS -->
    <style type="text/css">
        /* Map */
        #map {
            width: 100%;
            height: 480px;
            border-width: 1px;
            border-style: solid;
            border-color:#D2D6DE
        }

        /* Display scroll info on map */
        .map-scroll:before {
            color:white;
            content: 'Pressione Ctrl e role a tela simultaneamente para aplicar zoom ao mapa';
            position: absolute;
            padding: 15px;
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
            z-index: 999;
            font-size: 24px;
        }
        .map-scroll:after {
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            top: 0;
            content: '';
            background: #00000080;
            z-index: 998;
        }
    </style>
@endpush

<div class="form-group col-md-12">
    <i class="fas fa-question-circle" data-toggle="tooltip" style="cursor:help; margin-right:2.5px" title="{{ \Lang::get('text.map_info') }}"></i>
    {!! Form::label("map", \Lang::get("attributes.map").":") !!}
    <div id="map"></div>
</div>

@push('scripts')
    <!-- Leaflet v1.6.0 -->
    <script src="{{ asset('vendor/leaflet/leaflet.js') }}"></script>

    <!-- Esri Leaflet v2.3.2 -->
    <script src="{{ asset('vendor/leaflet/plugins/esri-leaflet.js') }}"></script>

    <!-- Esri Leaflet Geocoder v2.3.2 -->
    <script src="{{ asset('vendor/leaflet/plugins/esri-leaflet-geocoder.js') }}"></script>

    <!-- Map Scripts -->
    <script type="text/javascript">
        let disableFields = true;
        let statesAbbreviation = {"Acre":"AC", "Alagoas":"AL", "Amazonas":"AM", "Amapá":"AP", "Bahia":"BA", "Ceará":"CE", "Distrito Federal":"DF", "Espírito Santo":"ES", "Goiás":"GO", "Maranhão":"MA", "Minas Gerais":"MG", "Mato Grosso do Sul":"MS", "Mato Grosso":"MT", "Pará":"PA", "Paraíba":"PB", "Pernambuco":"PE", "Piauí":"PI", "Paraná":"PR", "Rio de Janeiro":"RJ", "Rio Grande do Norte":"RN", "Rondônia":"RO", "Roraima":"RR", "Rio Grande do Sul":"RS", "Santa Catarina":"SC", "Sergipe":"SE", "São Paulo":"SP", "Tocantins":"TO"};

        // Dynamically fill address
        $(document).ready(function() {
            $("#zipcode").on("change", function() {
                let zipcode = $(this).val();
                if (zipcode.indexOf("_")===-1) {
                    let route = "{{ route('address.findByZipcode', ':zipcode') }}";
                    route = route.replace(":zipcode", zipcode);

                    $.ajax({
                        url: route,
                        dataType: "json",
                        type: "GET",
                        beforeSend: function() {
                            $(".alert-ajax").fadeOut(300, "swing");
                            showLoading();
                        },
                        success: function(data) {
                            if (data) {
                                disableFields = false;

                                $("#address").val(data.logradouro);
                                $("#number").val("");
                                $("#neighborhood").val(data.bairro);
                                $("#city").val(data.localidade);
                                $("#state").val(data.uf);

                                updateMarker();
                            } else {
                                disableFields = true;

                                $(".alert-ajax").empty();
                                $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");
                                $(".alert-ajax").append("<li>{!! \Lang::get('flash.zipcode_not_found') !!}</li>");

                                window.scrollTo({top: 0, behavior: "smooth"});
                                $(".alert-ajax").fadeIn(300, "swing");
                            }
                        },
                        error: function(fail) {
                            disableFields = true;

                            $(".alert-ajax").empty();
                            $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");

                            if (typeof fail.responseJSON.errors !== "undefined") {
                                Object.values(fail.responseJSON.errors).forEach(function(value) {
                                    $(".alert-ajax").append("<li>"+value+"</li>");
                                });
                            } else {
                                $(".alert-ajax").append("<li>"+fail.responseJSON.message+"</li>");
                            }

                            window.scrollTo({top: 0, behavior: "smooth"});
                            $(".alert-ajax").fadeIn(300, "swing");
                        },
                        complete: function() {
                            toggleAddressFields();
                            hideLoading();
                        }
                    });
                } else {
                    disableFields = true;
                    toggleAddressFields();
                }
            });
        });

        // Enable address fields if zipcode is not empty
        $(document).ready(function() {
            if ($("#zipcode").val()) {
                disableFields = false;
                toggleAddressFields();
            }
        });

        // Map
        $(document).ready(function() {
            // Get lat lng from input
            latlng = L.latLng($("#latitude").val(), $("#longitude").val());

            // Initialize map
            map = L.map('map', {
                center: latlng,
                zoom: 17,
                scrollWheelZoom: false,
            });
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoiYmZvcnQ5NCIsImEiOiJja2FmZ2FxcGkwM3h6MnFsOHhzcXVmZGZiIn0.5tFveYNQ_HwzR_Mnq8Xl0g'
            }).addTo(map);

            // Make ctrl+scroll zoom map
            $("#map").bind('mousewheel DOMMouseScroll', function (event) {
                event.stopPropagation();
                if (event.ctrlKey == true) {
                    event.preventDefault();
                    map.scrollWheelZoom.enable();
                    $('#map').removeClass('map-scroll');
                    setTimeout(function(){
                        map.scrollWheelZoom.disable();
                    }, 1000);
                } else {
                    map.scrollWheelZoom.disable();
                    $('#map').addClass('map-scroll');
                    setTimeout(function(){
                        $('#map').removeClass('map-scroll');
                    }, 3000);
                }
            });

            // Remove ctrl+scroll info on click
            $("#map").bind('click', function (event) {
                $('#map').removeClass('map-scroll');
            });

            // Place marker
            marker = L.marker(latlng,{
                draggable: true,
                autoPan: true
            }).addTo(map);

            // Update address fields on marker drag
            marker.on("dragend", function (e) {
                lat = e.target.getLatLng().lat;
                lng = e.target.getLatLng().lng;
                latlng = L.latLng(lat, lng);
                updateAddress(latlng);
            });

            // Update marker position on address fields change
            $("#address, #neighborhood, #city, #state").on("change", function() {
                updateMarker();
            });
        });

        // Update marker location
        function updateMarker() {
            $(".alert-ajax").fadeOut(300, "swing");
            showLoading();

            address = $("#address").val();
            number = $("#number").val();
            neighborhood = $("#neighborhood").val();
            city = $("#city").val();
            state = $("#state").val();
            zipcode = $("#zipcode").val();

            L.esri.Geocoding.geocode().address(address+" "+number).neighborhood(neighborhood).city(city).region(state).postal(zipcode).run(function (error, result) {
                if (error) {
                    $(".alert-ajax").empty();
                    $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");
                    $(".alert-ajax").append("<li>{!! \Lang::get('flash.leaflet_marker') !!}</li>");
                    $(".alert-ajax").append("<li>"+status+"</li>");

                    window.scrollTo({top: 0, behavior: "smooth"});
                    $(".alert-ajax").fadeIn(300, "swing");

                } else {
                    latlng = L.latLng(result.results[0].latlng.lat, result.results[0].latlng.lng);
                    map.panTo(latlng); // Use "flyTo" to animate
                    marker.setLatLng(latlng);

                    $("#latitude").val(latlng.lat.toFixed(8));
                    $("#longitude").val(latlng.lng.toFixed(8));
                }

                hideLoading();
            });
        }

        // Update address input fields
        function updateAddress(latlng) {
            $(".alert-ajax").fadeOut(300, "swing");
            showLoading();

            L.esri.Geocoding.reverseGeocode().latlng(latlng).run(function (error, result) {
                if (error) {
                    $(".alert-ajax").empty();
                    $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");
                    $(".alert-ajax").append("<li>{!! \Lang::get('flash.leaflet_address') !!}</li>");
                    $(".alert-ajax").append("<li>"+status+"</li>");

                    window.scrollTo({top: 0, behavior: "smooth"});
                    $(".alert-ajax").fadeIn(300, "swing");

                } else {
                    if (result.address.PostalExt == "") {
                        $("#zipcode").val(result.address.Postal+"-000");
                    } else {
                        $("#zipcode").val(result.address.Postal);
                    }
                    $("#address").val(result.address.Address.replace(result.address.AddNum,"").trim());
                    $("#number").val(result.address.AddNum);
                    $("#neighborhood").val(result.address.District);
                    $("#city").val(result.address.City);
                    $("#state").val(statesAbbreviation[result.address.Region]);
                    $("#latitude").val(latlng.lat.toFixed(8));
                    $("#longitude").val(latlng.lng.toFixed(8));
                }

                hideLoading();
            });
        }

        // Disable or enable address input fields
        function toggleAddressFields() {
            if (disableFields) {
                $("#address").val("");
                $("#number").val("");
                $("#neighborhood").val("");
                $("#city").val("");
                $("#state").val("");
                $("#latitude").val("");
                $("#longitude").val("");

                $("#address").attr("readonly", true);
                $("#number").attr("readonly", true);
                $("#neighborhood").attr("readonly", true);
                $("#city").attr("readonly", true);
                $("#state").attr("readonly", true);

                latlng = L.latLng(0, 0);
                map.panTo(latlng);
                marker.setLatLng(latlng);
            } else {
                $("#address").attr("readonly", false);
                $("#number").attr("readonly", false);
                $("#neighborhood").attr("readonly", false);
                $("#city").attr("readonly", false);
                $("#state").attr("readonly", false);

                if ($("#number").val() == "") {
                    $("#number").focus();
                }
            }
        }
    </script>
@endpush