@if(\Auth::user()->hasRole('admin'))
    {{-- Users --}}
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}"><i class="fa fa-users"></i><span>{!! \Lang::choice('tables.users','p') !!}</span></a>
    </li>

    {{-- Wifi Types --}}
    <li class="{{ Request::is('wifi-types*') ? 'active' : '' }}">
        <a href="{{ route('wifiTypes.index') }}"><i class="fa fa-wifi"></i><span>{!! \Lang::choice('tables.wifi_types','p') !!}</span></a>
    </li>

    {{-- Spaces --}}
    <li class="{{ Request::is('spaces*') ? 'active' : '' }}">
        <a href="{{ route('spaces.index') }}"><i class="fa fa-location-arrow"></i><span>{!! \Lang::choice('tables.spaces','p') !!}</span></a>
    </li>

    {{-- Grades --}}
    <li class="{{ Request::is('grades*') ? 'active' : '' }}">
        <a href="{{ route('grades.index') }}"><i class="fa fa-star"></i><span>{!! \Lang::choice('tables.grades','p') !!}</span></a>
    </li>
@else
    {{-- Dashboard --}}
    <li class="{{ Request::is('dashboard*') ? 'active' : '' }}">
        <a href="{{ route('users.dashboard') }}"><i class="fa fa-columns"></i><span>{!! \Lang::get('text.dashboard') !!}</span></a>
    </li>

    {{-- Users --}}
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('user.users.edit', [\Auth::user()->id]) }}"><i class="fa fa-user"></i><span>Perfil</span></a>
    </li>

    {{-- Spaces --}}
    <li class="{{ Request::is('spaces/create*') ? 'active' : '' }}">
        <a href="{{ route('user.spaces.create') }}"><i class="fa fa-location-arrow"></i><span>{!! \Lang::get("text.add") !!}  {!! mb_strtolower(\Lang::choice("tables.spaces", "s"), "UTF-8") !!}</span></a>
    </li>

    {{-- Grades --}}
    <li class="{{ Request::is('grades*') ? 'active' : '' }}">
        <a href="{{ route('user.grades.index') }}"><i class="fa fa-star"></i><span>Meus feedbacks</span></a>
    </li>
@endif
