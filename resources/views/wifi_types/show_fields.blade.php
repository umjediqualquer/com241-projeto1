<!-- Name Field -->
@isset($wifiType->name)
    <div class="form-group">
        {!! Form::label('name', \Lang::get("attributes.name").":") !!}
        <p>{{ $wifiType->name }}</p>
    </div>
@endisset

<!-- Description Field -->
@isset($wifiType->description)
    <div class="form-group">
        {!! Form::label('description', \Lang::get("attributes.description").":") !!}
        <p>{{ $wifiType->description }}</p>
    </div>
@endisset

<!-- Created At Field -->
@isset($wifiType->readable_created_at)
    <div class="form-group">
        {!! Form::label('created_at', \Lang::get("attributes.created_at").":") !!}
        <p>{{ $wifiType->readable_created_at }}</p>
    </div>
@endisset

<!-- Updated At Field -->
@isset($wifiType->readable_updated_at)
    <div class="form-group">
        {!! Form::label('updated_at', \Lang::get("attributes.updated_at").":") !!}
        <p>{{ $wifiType->readable_updated_at }}</p>
    </div>
@endisset
