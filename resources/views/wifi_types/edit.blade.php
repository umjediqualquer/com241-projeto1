@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {!! mb_strtoupper(\Lang::get("text.edit"), "UTF-8") !!} {!! mb_strtoupper(\Lang::choice("tables.wifi_types", "s"), "UTF-8") !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($wifiType, ['route' => ['wifiTypes.update', $wifiType->id], 'method' => 'patch']) !!}

                        @include('wifi_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection