@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {!! mb_strtoupper(\Lang::choice("text.details_of", "m"), "UTF-8") !!} {!! mb_strtoupper(\Lang::choice("tables.wifi_types", "s"), "UTF-8") !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('wifi_types.show_fields')
                    <a href="{{ route('wifiTypes.index') }}" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
