<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', \Lang::get("attributes.name").":") !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', \Lang::get("attributes.description").":") !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(\Lang::get("text.save"), ['class' => 'btn-orange']) !!}
    <a href="{{ route('wifiTypes.index') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
</div>
