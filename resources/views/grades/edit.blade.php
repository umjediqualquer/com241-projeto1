@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {!! mb_strtoupper(\Lang::get("text.edit"), "UTF-8") !!} {!! mb_strtoupper(\Lang::choice("tables.grades", "s"), "UTF-8") !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row basic-font">
                    @if(\Auth::user()->hasRole('admin'))
                        {!! Form::model($grade, ['route' => ['grades.update', $grade->id], 'method' => 'patch']) !!}
                    @else
                        {!! Form::model($grade, ['route' => ['user.grades.update', $grade->id], 'method' => 'patch']) !!}
                    @endif

                        @include('grades.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection