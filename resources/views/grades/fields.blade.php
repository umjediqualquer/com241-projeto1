@if(\Auth::user()->hasRole('admin'))
    <!-- User Id Field -->
    <div class="form-group col-md-6">
        {!! Form::label('user_id', \Lang::get("attributes.user_id").":") !!}
        {!! Form::select('user_id', ['' => 'Selecionar'] + $usersArray, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Space Id Field -->
    <div class="form-group col-md-6">
        {!! Form::label('space_id', \Lang::get("attributes.space_id").":") !!}
        {!! Form::select('space_id', ['' => 'Selecionar'] + $spacesArray, null, ['class' => 'form-control']) !!}
    </div>
@else
    <!-- User Id Field -->
    {{ Form::hidden('user_id', \Auth::user()->id) }}

    <!-- Space Id Field -->
    <div class="form-group col-md-12">
        {!! Form::label('space_id', \Lang::get("attributes.space_id").":") !!}
        {!! Form::select('space_id', ['' => 'Selecionar'] + $spacesArray, null, ['class' => 'form-control', Request::is('*edit*')? 'disabled' : '']) !!}
    </div>
@endif

<!-- Rating Field -->
<div class="form-group col-md-6">
    {!! Form::label('rating', \Lang::get("attributes.rating").":") !!}
    <input type="number" name="rating" id="rating" class="form-control grade-rating">
</div>

<!-- Wifi Speed Field -->
<div class="form-group col-md-6">
    <i class="fas fa-question-circle" data-toggle="tooltip" style="cursor:help; margin-right:2.5px" title="Clique no link ao lado para abrir um teste de velocidade"></i>
    <a>{!! Form::label('wifi_speed', \Lang::get("attributes.wifi_speed").":", ['class' => 'wifi-speed-test']) !!}</a>
    {!! Form::text('wifi_speed', null, ['class' => 'form-control float-mask']) !!}
</div>

<!-- Comments Field -->
<div class="form-group col-md-12">
    {!! Form::label('comments', \Lang::get("attributes.comments").":") !!}
    {!! Form::textarea('comments', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(\Lang::get("text.save"), ['class' => 'btn-orange']) !!}
    @if(\Auth::user()->hasRole('admin'))
        <a href="{{ route('grades.index') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @else
        <a href="{{ route('user.grades.index') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @endif
</div>

@push('scripts')
    <script type="text/javascript">
        @role('admin')
            $("[name='user_id']").select2();
        @endrole
        $("[name='space_id']").select2();

        $(document).ready(function() {
            $('.grade-rating').rating({
                language:"pt-BR",
                theme:"krajee-svg",
                stars:5,
                min:0,
                max:5,
                step:1,
                showClear:false,
            });

            @isset($grade)
                var value = @json($grade->rating);
                $('.grade-rating').rating('update', value);
            @endisset

            @if(request()->get("space_id") && Request::is('*create*'))
                spaceId = @json(request()->get("space_id"));
                $("#space_id").val(spaceId).change();
            @endif

            // Open speed test tab
            $(document).on("click", ".wifi-speed-test", function () {
                window.open('https://fast.com/','_blank');
            });
        });
    </script>
@endpush
