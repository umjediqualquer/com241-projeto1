@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {!! mb_strtoupper(\Lang::choice("text.details_of", "m"), "UTF-8") !!} {!! mb_strtoupper(\Lang::choice("tables.grades", "s"), "UTF-8") !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row basic-font" style="padding: 1.8rem">
                    @include('grades.show_fields')
                    @if(\Auth::user()->hasRole('admin'))
                        <a href="{{ route('grades.index') }}" class="btn-blue">Voltar</a>
                    @else
                        <a href="{{ route('user.grades.index') }}" class="btn-blue">Voltar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
