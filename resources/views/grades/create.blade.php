@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        {!! mb_strtoupper(\Lang::get("text.add"), "UTF-8") !!} {!! mb_strtoupper(\Lang::choice("tables.grades", "s"), "UTF-8") !!}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @if(\Auth::user()->hasRole('admin'))
                        {!! Form::open(['route' => 'grades.store']) !!}
                    @else
                        {!! Form::open(['route' => 'user.grades.store']) !!}
                    @endif

                        @include('grades.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
