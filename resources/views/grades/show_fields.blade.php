<!-- User Id Field -->
@isset($grade->user_id)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('user_id', \Lang::get("attributes.user_id").":") !!}
        <p>{{ $grade->user->name }}</p>
    </div>
@endisset

<!-- Space Id Field -->
@isset($grade->space_id)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('space_id', \Lang::get("attributes.space_id").":") !!}
        <p>{{ $grade->space->name }}</p>
    </div>
@endisset

<!-- Rating Field -->
@isset($grade->rating)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('rating', \Lang::get("attributes.rating").":") !!}
        <p>{{ $grade->rating }}</p>
    </div>
@endisset

<!-- Wifi Speed Field -->
@isset($grade->readable_wifi_speed)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('wifi_speed', \Lang::get("attributes.wifi_speed").":") !!}
        <p>{{ $grade->readable_wifi_speed }}</p>
    </div>
@endisset

<!-- Comments Field -->
@isset($grade->comments)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('comments', \Lang::get("attributes.comments").":") !!}
        <p>{{ $grade->comments }}</p>
    </div>
@endisset

<!-- Created At Field -->
@isset($grade->readable_created_at)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('created_at', \Lang::get("attributes.created_at").":") !!}
        <p>{{ $grade->readable_created_at }}</p>
    </div>
@endisset

<!-- Updated At Field -->
@isset($grade->readable_updated_at)
    <div class="form-group col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('updated_at', \Lang::get("attributes.updated_at").":") !!}
        <p>{{ $grade->readable_updated_at }}</p>
    </div>
@endisset
