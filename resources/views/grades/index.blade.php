@extends('layouts.app')

@section('content')
    <section class="content-header basic-font">
        <h1 class="pull-left" style="margin-bottom: 1.2rem">{!! mb_strtoupper(\Lang::choice("tables.grades", "p"), "UTF-8") !!}</h1>
        @role('admin')
            <h1 class="pull-right">
                <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('grades.create') }}">{!! mb_strtoupper(\Lang::get("text.add"), "UTF-8") !!}</a>
            </h1>
        @endrole
    </section>
    <div class="content basic-font">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('grades.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
