<!-- Wifi Type Id Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('wifi_type_id', \Lang::get("attributes.wifi_type_id").":") !!}
    {!! Form::select('wifi_type_id', ['' => 'Selecionar'] + $wifiTypesArray, null, ['class' => 'form-control']) !!}
</div>

@if(\Auth::user()->hasRole('admin'))
    <!-- User Id Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('user_id', \Lang::get("attributes.user_id").":") !!}
        {!! Form::select('user_id', ['' => 'Selecionar'] + $usersArray, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Name Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('name', \Lang::get("attributes.name").":") !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Wifi Name Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('wifi_name', \Lang::get("attributes.wifi_name").":") !!}
        {!! Form::text('wifi_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Wifi Password Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('wifi_password', \Lang::get("attributes.wifi_password").":") !!}
        {!! Form::text('wifi_password', null, ['class' => 'form-control']) !!}
    </div>
@else
    <!-- User Id Field -->
    {{ Form::hidden('user_id', \Auth::user()->id) }}

    <!-- Name Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('name', \Lang::get("attributes.name").":") !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Wifi Name Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('wifi_name', \Lang::get("attributes.wifi_name").":") !!}
        {!! Form::text('wifi_name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Wifi Password Field -->
    <div class="form-group col-sm-6 col-md-4">
        {!! Form::label('wifi_password', \Lang::get("attributes.wifi_password").":") !!}
        {!! Form::text('wifi_password', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Wifi Speed Field -->
<div class="form-group col-sm-6 col-md-4">
    <i class="fas fa-question-circle" data-toggle="tooltip" style="cursor:help; margin-right:2.5px" title="Clique no link ao lado para abrir um teste de velocidade"></i>
    <a>{!! Form::label('wifi_speed', \Lang::get("attributes.wifi_speed").":", ['class' => 'wifi-speed-test']) !!}</a>
    {!! Form::text('wifi_speed', null, ['class' => 'form-control float-mask']) !!}
</div>

<!-- Sockets Number Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('sockets_number', \Lang::get("attributes.sockets_number").":") !!}
    {!! Form::text('sockets_number', null, ['class' => 'form-control integer-mask']) !!}
</div>

<!-- Zipcode Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('zipcode', \Lang::get("attributes.zipcode").":") !!}
    {!! Form::text('zipcode', null, ['class' => 'form-control zipcode-mask']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('address', \Lang::get("attributes.address").":") !!}
    {!! Form::text('address', null, ['class' => 'form-control', "readonly" => true]) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('number', \Lang::get("attributes.number").":") !!}
    {!! Form::text('number', null, ['class' => 'form-control', "readonly" => true]) !!}
</div>

<!-- Neighborhood Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('neighborhood', \Lang::get("attributes.neighborhood").":") !!}
    {!! Form::text('neighborhood', null, ['class' => 'form-control', "readonly" => true]) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('city', \Lang::get("attributes.city").":") !!}
    {!! Form::text('city', null, ['class' => 'form-control', "readonly" => true]) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6 col-md-4">
    {!! Form::label('state', \Lang::get("attributes.state").":") !!}
    {!! Form::text('state', null, ['class' => 'form-control state-mask', "readonly" => true]) !!}
</div>

<!-- Complement Field -->
<div class="form-group col-12 col-sm-6 col-lg-6">
    {!! Form::label('complement', \Lang::get("attributes.complement").":") !!}
    {!! Form::textarea('complement', null, ['class' => 'form-control']) !!}
</div>

<!-- Reference Field -->
<div class="form-group col-12 col-sm-6 col-lg-6">
    {!! Form::label('reference', \Lang::get("attributes.reference").":") !!}
    {!! Form::textarea('reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Map -->
@include("layouts.map")

<!-- Latitude Field -->
<div class="form-group col-12 col-sm-6 col-lg-6">
    {!! Form::label('latitude', \Lang::get("attributes.latitude").":") !!}
    {!! Form::text('latitude', null, ['class' => 'form-control latitude-mask', "readonly" => true]) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-12 col-sm-6 col-lg-6">
    {!! Form::label('longitude', \Lang::get("attributes.longitude").":") !!}
    {!! Form::text('longitude', null, ['class' => 'form-control longitude-mask', "readonly" => true]) !!}
</div>

<!-- Submit Field -->
<div class="col-md-12">
    {!! Form::submit(\Lang::get("text.save"), ['class' => 'btn-orange']) !!}
    @if(\Auth::user()->hasRole('admin'))
        <a href="{{ route('spaces.index') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @else
        <a href="{{ route('users.dashboard') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @endif
</div>

@push('scripts')
    <script type="text/javascript">
        $("[name='wifi_type_id']").select2();
        @role('admin')
            $("[name='user_id']").select2();
        @endrole

        // Actions on page load
        $(window).on("pageshow", function() {
            $("#wifi_type_id").trigger("change");
        });

        // Disable or enable password field
        $(document).on("change", "#wifi_type_id", function () {
            let wifiTypeId = $(this).val();

            if (wifiTypeId) {
                if (wifiTypeId == '1' || wifiTypeId == '3') {
                    $("#wifi_password").prop("disabled", true);
                } else if (wifiTypeId == '2') {
                    $("#wifi_password").prop("disabled", false);
                }
            }
        });

        // Open speed test tab
        $(document).on("click", ".wifi-speed-test", function () {
            window.open('https://fast.com/','_blank');
        });
    </script>
@endpush
