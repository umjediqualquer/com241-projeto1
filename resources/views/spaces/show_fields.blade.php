<!-- Wifi Type Id Field -->
@isset($space->wifi_type_id)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('wifi_type_id', \Lang::get("attributes.wifi_type_id").":") !!}
        <p>{{ $space->wifiType->name }}</p>
    </div>
@endisset

<!-- User Id Field -->
@isset($space->user_id)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('user_id', \Lang::get("attributes.user_id").":") !!}
        <p>{{ $space->user->name }}</p>
    </div>
@endisset

<!-- Name Field -->
@isset($space->name)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('name', \Lang::get("attributes.name").":") !!}
        <p>{{ $space->name }}</p>
    </div>
@endisset

<!-- Wifi Name Field -->
@isset($space->wifi_name)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('wifi_name', \Lang::get("attributes.wifi_name").":") !!}
        <p>{{ $space->wifi_name }}</p>
    </div>
@endisset

<!-- Wifi Password Field -->
@isset($space->wifi_password)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('wifi_password', \Lang::get("attributes.wifi_password").":") !!}
        <p>{{ $space->wifi_password }}</p>
    </div>
@endisset

<!-- Wifi Speed Field -->
@isset($space->readable_wifi_speed)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('wifi_speed', \Lang::get("attributes.wifi_speed").":") !!}
        <p>{{ $space->readable_wifi_speed }}</p>
    </div>
@endisset

<!-- Sockets Number Field -->
@isset($space->sockets_number)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('sockets_number', \Lang::get("attributes.sockets_number").":") !!}
        <p>{{ $space->sockets_number }}</p>
    </div>
@endisset

<!-- Zipcode Field -->
@isset($space->zipcode)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('zipcode', \Lang::get("attributes.zipcode").":") !!}
        <p>{{ $space->zipcode }}</p>
    </div>
@endisset

<!-- Address Field -->
@isset($space->address)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('address', \Lang::get("attributes.address").":") !!}
        <p>{{ $space->address }}</p>
    </div>
@endisset

<!-- Number Field -->
@isset($space->number)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('number', \Lang::get("attributes.number").":") !!}
        <p>{{ $space->number }}</p>
    </div>
@endisset

<!-- Neighborhood Field -->
@isset($space->neighborhood)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('neighborhood', \Lang::get("attributes.neighborhood").":") !!}
        <p>{{ $space->neighborhood }}</p>
    </div>
@endisset

<!-- City Field -->
@isset($space->city)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('city', \Lang::get("attributes.city").":") !!}
        <p>{{ $space->city }}</p>
    </div>
@endisset

<!-- State Field -->
@isset($space->state)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('state', \Lang::get("attributes.state").":") !!}
        <p>{{ $space->state }}</p>
    </div>
@endisset

<!-- Complement Field -->
@isset($space->complement)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('complement', \Lang::get("attributes.complement").":") !!}
        <p>{{ $space->complement }}</p>
    </div>
@endisset

<!-- Reference Field -->
@isset($space->reference)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('reference', \Lang::get("attributes.reference").":") !!}
        <p>{{ $space->reference }}</p>
    </div>
@endisset

<!-- Latitude Field -->
@isset($space->readable_latitude)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('latitude', \Lang::get("attributes.latitude").":") !!}
        <p>{{ $space->readable_latitude }}</p>
    </div>
@endisset

<!-- Longitude Field -->
@isset($space->readable_longitude)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('longitude', \Lang::get("attributes.longitude").":") !!}
        <p>{{ $space->readable_longitude }}</p>
    </div>
@endisset

<!-- Created At Field -->
@isset($space->readable_created_at)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('created_at', \Lang::get("attributes.created_at").":") !!}
        <p>{{ $space->readable_created_at }}</p>
    </div>
@endisset

<!-- Updated At Field -->
@isset($space->readable_updated_at)
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label('updated_at', \Lang::get("attributes.updated_at").":") !!}
        <p>{{ $space->readable_updated_at }}</p>
    </div>
@endisset

{{-- Grades Field --}}
@if($space->grades->isNotEmpty())
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-4">
        {!! Form::label("grade_id", "Feedbacks") !!}
        <table class="table table-bordered table-striped table-content-size table-scroll-x">
            <thead>
                <tr>
                    <th>{!! Form::label("user_id", \Lang::get("attributes.user_id"), ["class" => "no-margin"]) !!}</th>
                    <th>{!! Form::label("rating", \Lang::get("attributes.rating"), ["class" => "no-margin"]) !!}</th>
                    <th>{!! Form::label("wifi_speed", \Lang::get("attributes.wifi_speed"), ["class" => "no-margin"]) !!}</th>
                    <th>{!! Form::label("comments", \Lang::get("attributes.comments"), ["class" => "no-margin"]) !!}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($space->grades as $grade)
                    <tr>
                        <td>{{ $grade->user->name }}</td>
                        <td>{{ $grade->rating }}</td>
                        <td>{{ $grade->readable_wifi_speed}}</td>
                        <td>{{ $grade->comments }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif
