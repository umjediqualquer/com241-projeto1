@extends('layouts.app')

@section('content')
    <div class="content" id="fields-body">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row basic-font input-style">
                    @if(\Auth::user()->hasRole('admin'))
                        {!! Form::open(['route' => 'spaces.store']) !!}
                    @else
                        {!! Form::open(['route' => 'user.spaces.store']) !!}
                    @endif

                        @include('spaces.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
