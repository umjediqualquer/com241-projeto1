@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row basic-font" style="padding: 2.4rem">
                    @include('spaces.show_fields')
                    @if(\Auth::user()->hasRole('admin'))
                        <a href="{{ route('spaces.index') }}" class="btn-blue">Voltar</a>
                    @else
                        <a href="{{ route('users.dashboard') }}" class="btn-blue">Voltar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
