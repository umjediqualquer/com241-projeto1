<!-- Role Name Field -->
@isset($user->readable_role_name)
    {{-- Role Field --}}
    {!! Form::label('name', \Lang::get('attributes.role_name').':') !!}
    <p>{!! $user->readable_role_name !!}</p>
@endisset

<!-- Name Field -->
@isset($user->name)
    <div class="form-group">
        {!! Form::label('name', \Lang::get("attributes.name").":") !!}
        <p>{{ $user->name }}</p>
    </div>
@endisset

<!-- Email Field -->
@isset($user->email)
    <div class="form-group">
        {!! Form::label('email', \Lang::get("attributes.email").":") !!}
        <p>{{ $user->email }}</p>
    </div>
@endisset

<!-- Is Active Field -->
@isset($user->readable_is_active)
    <div class="form-group">
        {!! Form::label('is_active', \Lang::get("attributes.is_active").":") !!}
        <p>{{ $user->readable_is_active }}</p>
    </div>
@endisset

<!-- Created At Field -->
@isset($user->readable_created_at)
    <div class="form-group">
        {!! Form::label('created_at', \Lang::get("attributes.created_at").":") !!}
        <p>{{ $user->readable_created_at }}</p>
    </div>
@endisset

<!-- Updated At Field -->
@isset($user->readable_updated_at)
    <div class="form-group">
        {!! Form::label('updated_at', \Lang::get("attributes.updated_at").":") !!}
        <p>{{ $user->readable_updated_at }}</p>
    </div>
@endisset
