@extends('layouts.app')

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row basic-font" style="padding: 2.4rem">
                    @if(\Auth::user()->hasRole('admin'))
                        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                    @else
                        {!! Form::model($user, ['route' => ['user.users.update', $user->id], 'method' => 'patch']) !!}
                    @endif

                        @include('users.fields')

                    {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection