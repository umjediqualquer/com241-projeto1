@extends('layouts.app')

@section('content')
    {{-- <section class="content-header">
        <!-- <h1 class="pull-left">{!! mb_strtoupper(\Lang::choice("tables.spaces", "p"), "UTF-8") !!}</h1> -->
        <!-- <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="#">{!! mb_strtoupper(\Lang::get("text.add"), "UTF-8") !!}</a>
        </h1> -->
    </section> --}}

    <div id="dashboard-body">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="search-container">
            <div class="input-block">
                <i class="glyphicon glyphicon-search"></i>
                <input class="space-search" type="#" name="#" placeholder="Pesquisar lugares..." required>
            </div>
            <div>
                <button class="btn-primary">
                    <a href="{{ route('user.spaces.create') }}"><i class="glyphicon glyphicon-plus"></i>Adicionar espaço</a>
                </button>
            </div>
        </div>

        <div class="cards-container">
            @foreach($spaces as $space)
                <article class="card-item" data-space-name="{{ $space->name }}">
                    <header>
                        <h1>{{ $space->name }}</h1>
                        <h4>{{ $space->wifiType->name }}</h4>

                        <div>
                            <span>
                                <i class="glyphicon glyphicon-map-marker"></i><a href="{{ $space->maps_url }}" target="_blank" data-toggle="tooltip" data-placement="top" title="{{ $space->full_address }}">direções</a>
                            </span>
                            <span class="rate-space" data-space-id="{{ $space->id }}">
                                @for($fullStar = 0; $fullStar < $space->fullStars; $fullStar++)
                                    <i class="glyphicon glyphicon-star"></i>
                                @endfor
                                @for($emptyStar = 0; $emptyStar < $space->emptyStars; $emptyStar++)
                                    <i class="glyphicon glyphicon-star-empty"></i>
                                @endfor
                            </span>
                        </div>
                    </header>

                    <div class="card-info">
                        <i class="glyphicon glyphicon-phone"></i>{{ $space->wifi_name }}<br>
                        <i class="glyphicon glyphicon-lock"></i>{{ $space->wifi_password? $space->wifi_password:'[sem senha]' }}<br>
                        <i class="glyphicon glyphicon-flash"></i>{{ $space->wifi_speed }} Mbps<br>
                        <div class="card-info-footer">
                            <span><i class="glyphicon glyphicon-sound-dolby"></i>{{ $space->sockets_number }}</span>
                            <a href="{{ route('user.spaces.show', [$space->id]) }}" class="btn-secondary">Ver detalhes</a>
                        </div>
                    </div>

                </article>
            @endforeach
        </div>
    </div>

    <!-- modal p/ adicionar novo local -->
    <div class="modal fade" id="addNewPlace" tabindex="-1" role="dialog" aria-labelledby="addNewPlace" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addNewPlaceTitle">Adicionar novo local</h5>
                </div>
                <div class="modal-body">
                    <div class="input-block">
                        <input type="email" name="email" placeholder="Nome" required>
                    </div>
                    <div class="input-block">
                        <input type="email" name="email" placeholder="Nome" required>
                    </div>
                    <div class="input-block">
                        <input type="email" name="email" placeholder="Nome" required>
                    </div>
                    <div class="input-block">
                        <input type="email" name="email" placeholder="Nome" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary2" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary2">Confirmar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal p/ info do local -->
    <div class="modal fade" id="placeDetails" tabindex="-1" role="dialog" aria-labelledby="placeDetails" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="placeDetails">Detalhes</h5>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary2" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        // Redirect to correct page
        $(document).on("click", ".rate-space", function () {
            let spaceId = $(this).attr("data-space-id");

            if (spaceId) {
                let route = "{{ route('user.spaces.hasUserRated', [':space_id']) }}";
                route = route.replace(":space_id", spaceId);

                $.ajax({
                    url: route,
                    dataType: "json",
                    type: "GET",
                    beforeSend: function() {
                        $(".alert-ajax").fadeOut(300, "swing");
                    },
                    success: function(data) {
                        if (data.hasUserRated) {
                            let route = "{{ route('user.grades.edit', [':grade_id']) }}";
                            route = route.replace(":grade_id", data.gradeId);
                            window.location = route;
                        } else {
                            let route = "{{ route('user.grades.create') }}"
                            route = route+"?space_id="+spaceId;
                            window.location = route;
                        }
                    },
                    error: function(fail) {
                        $(".alert-ajax").empty();
                        $(".alert-ajax").attr("class", "alert alert-danger alert-ajax");

                        if (typeof fail.responseJSON.errors !== "undefined") {
                            Object.values(fail.responseJSON.errors).forEach(function(value) {
                                $(".alert-ajax").append("<li>"+value+"</li>");
                            });
                        } else {
                            $(".alert-ajax").append("<li>"+fail.responseJSON.message+"</li>");
                        }

                        window.scrollTo({top: 0, behavior: "smooth"});
                        $(".alert-ajax").fadeIn(300, "swing");
                    }
                });
            }
        });

        $(document).on("keyup", ".space-search", function () {
            if($(this).val()) {
                searchQuery = $(this).val();
                $(".card-item").each(function(i, elem) {
                    spaceName = $(elem).attr("data-space-name").toLowerCase();
                    if (spaceName.includes(searchQuery)) {
                        $(elem).show();
                    } else {
                        $(elem).hide();
                    }
                });
            } else {
                $(".card-item").each(function(i, elem) {
                    $(elem).show();
                });
            }
        });
    </script>
@endpush
