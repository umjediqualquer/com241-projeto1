@role('admin')
    <!-- Role Name Field -->
    <div class="form-group col-md-6">
        {!! Form::label("role_name", \Lang::get("attributes.role_name").":") !!}
        {!! Form::select("role_name", ["" => "Selecionar"] + $rolesArray, null, ["class" => "form-control"]) !!}
    </div>
@endrole

<!-- Name Field -->
<div class="name-field form-group">
    {!! Form::label("name", \Lang::get("attributes.name").":") !!}
    {!! Form::text("name", null, ["class" => "form-control"]) !!}
</div>

@isset($user)
    {{-- Keep Password --}}
    <div class="form-group col-md-12">
        <div class="icheck">
            <label>
                {!! Form::checkbox('keep_password', 1, true, ['id' => 'keep_password']) !!}
                <span>{!! \Lang::get('attributes.keep_password') !!}</span>
            </label>
        </div>
    </div>
@endisset

<!-- Password Fields -->
<div class="password-fields">
    <!-- Password Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('password', \Lang::get("attributes.password").":") !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <!-- Password Confirmation Field -->
    <div class="form-group col-md-6">
        {!! Form::label('password_confirmation', \Lang::get('attributes.password_confirmation').':') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', \Lang::get("attributes.email").":") !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-md-12">
    {!! Form::label("is_active", \Lang::get("attributes.is_active").":") !!}
    <div class="icheck">
        <label>
            {!! Form::radio('is_active', 1, true) !!}
            <span>{!! \Lang::get('text.yes') !!}</span>
        </label>
    </div>
    <div class="icheck">
        <label>
            {!! Form::radio('is_active', 0, false) !!}
            <span>{!! \Lang::get('text.no') !!}</span>
        </label>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(\Lang::get("text.save"), ['class' => 'btn-orange']) !!}
    @if(\Auth::user()->hasRole('admin'))
        <a href="{{ route('users.index') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @else
        <a href="{{ route('users.dashboard') }}" class="btn-blue">{{ \Lang::get("text.cancel") }}</a>
    @endif

</div>

@push('scripts')
    <script type="text/javascript">
        @if(\Auth::user()->hasRole('admin'))
            $("[name='role_name']").select2();
            $(".name-field").addClass("col-md-6");
        @else
            $(".name-field").addClass("col-md-12");
        @endif

        // Show password fields
        $(document).on("change", "#keep_password", function(){
            if ($(this).is(":checked")) {
                $(".password-fields").hide();
            } else {
                $(".password-fields").show();
            }
        });
        @isset($user)
            $("#keep_password").trigger("change");
        @endisset
    </script>
@endpush