<?php

return [

    'grades'                      => '[s] Feedback                       |[p] Feedbacks',
    'spaces'                      => '[s] Espaço                         |[p] Espaços',
    'users'                       => '[s] Usuário                        |[p] Usuários',
    'wifi_types'                  => '[s] Tipo de Wi-Fi                  |[p] Tipos de Wi-Fi',

];
