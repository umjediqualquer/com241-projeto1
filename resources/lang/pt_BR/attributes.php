<?php

return [
    // User
    'id'                       => 'Id',
    'name'                     => 'Nome',
    'email'                    => 'E-mail',
    'password'                 => 'Senha',
    'password_confirmation'    => 'Confirmar Senha',
    'keep_password'            => 'Manter Senha',
    'role_name'                => 'Perfil',
    'is_active'                => 'Ativo',
    'email_verified_at'        => 'E-mail Verificado em',
    'created_at'               => 'Criado em',
    'updated_at'               => 'Atualizado em',

    // WifiType
    'description'              => 'Descrição',

    // Space
    'wifi_name'                => 'Nome do Wi-Fi (SSID)',
    'wifi_password'            => 'Senha do Wi-Fi',
    'wifi_speed'               => 'Velocidade do Wi-Fi (Mbps)',
    'sockets_number'           => 'Número de Tomadas',
    'address'                  => 'Endereço',
    'number'                   => 'Número',
    'neighborhood'             => 'Bairro',
    'city'                     => 'Cidade',
    'state'                    => 'Estado',
    'zipcode'                  => 'CEP',
    'complement'               => 'Complemento',
    'reference'                => 'Referência',
    'latitude'                 => 'Latitude',
    'longitude'                => 'Longitude',
    'full_address'             => 'Endereço Completo',
    'map'                      => 'Mapa',

    // Grade
    'rating'                   => 'Nota',
    'comments'                 => 'Comentários',

    // FK
    'wifi_type_id'             => 'Tipo de Wi-Fi',
    'user_id'                  => 'Usuário',
    'space_id'                 => 'Espaço',
];
