<?php

return [

    // Actions
    'save'                  => 'Salvar',
    'cancel'                => 'Cancelar',
    'back'                  => 'Voltar',
    'add'                   => 'Adicionar',
    'remove'                => 'Remover',
    'edit'                  => 'Editar',
    'details'               => 'Detalhes',
    'details_of'            => '[f] Detalhes da |[m] Detalhes do',
    'search'                => 'Pesquisar',
    'filter'                => 'Filtrar',
    'check'                 => 'Dispensar',
    'sell'                  => 'Vender',
    'redeem'                => 'Resgatar',


    // Messages
    'confirmation'          => 'Você tem certeza?',
    'yes'                   => 'Sim',
    'no'                    => 'Não',
    'member_since'          => 'Membro desde',
    'map_info'              => 'Arraste o marcador para atualizar os campos de endereço.',


    // Menu
    'home'                  => 'Início',
    'profile'               => 'Perfil',
    'dashboard'             => 'Dashboard',
    'logout'                => 'Sair',

];
